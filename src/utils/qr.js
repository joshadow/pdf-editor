import QRCode from 'qrcode'

const createQr = async (url) => {
    var qr = await QRCode.toDataURL(url, {
        margin: 0,
        scale: 2,
    });
    return qr;
}

export { createQr }
