import axios from 'axios';
const softexpertApi = "https://apisoftexpert.servicios.mitur.gob.do";

const saveDocumentData = async ( documentData, request, document ) =>{ 
    const url = `${softexpertApi}/api/documents/savedata`;

    var result = null;
    const data = {
        data: documentData,
        request_id: request,
        document_id: document
    }
    await axios.post( url, data ).then( r => {
        result = r.data
    })

    return result;
}

export { saveDocumentData }